#!/bin/bash
echo "SLEEPING"
sleep 5
echo "INITIALIZING CENTRAL DATA REPLICATION"
mongosh -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD <<EOF
disableTelemetry()
var config = {
  "_id": "mongoSet",
  "members": [
      { "_id": 0, "host": "$MAIN_CENTRAL_HOST:$MAIN_CENTRAL_PORT" },
      { "_id": 1, "host": "$FIRST_CENTRAL_REPLICA_HOST:$FIRST_CENTRAL_REPLICA_PORT" },
      { "_id": 2, "host": "$SECOND_CENTRAL_REPLICA_HOST:$SECOND_CENTRAL_REPLICA_PORT" }
  ]
};
rs.initiate(config, {
    force: true
});
rs.status();
EOF