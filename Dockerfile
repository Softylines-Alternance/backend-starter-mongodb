FROM node:14-alpine
WORKDIR /app
RUN yarn set version 3.6.1
COPY package.json yarn.lock .yarn .yarnrc.yml ./ 
RUN  yarn install
COPY . ./
EXPOSE 6003
ENTRYPOINT ["yarn","run","start:dev"]
