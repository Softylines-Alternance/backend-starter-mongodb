import { MAIL_SERVICE_PROVIDER_NAME } from '../../providers.constants';
import { MailingService } from '../services/mailing.service';

export const mailServiceProviders = [
  {
    useClass: MailingService,
    provide: MAIL_SERVICE_PROVIDER_NAME,
  },
];
