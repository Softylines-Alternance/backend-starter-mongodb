export interface IEmailOptions {
  target: string;
  subject: string;
  template: string;
}
