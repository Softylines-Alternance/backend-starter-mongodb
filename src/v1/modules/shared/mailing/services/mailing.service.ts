import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { IEmailOptions } from '../mailing.interface';

@Injectable()
export class MailingService {
  constructor(private mailerService: MailerService) {}
  async sendMail(emailOptions: IEmailOptions) {
    const { target, subject, template, ...dynamicFields } = emailOptions;
    const emailTarget = (process.env.NODE_ENV = 'development'
      ? process.env.TEST_EMAIL_RECEIVER
      : target);
    try {
      return await this.mailerService.sendMail({
        to: emailTarget,
        subject: subject,
        context: dynamicFields,
        template: template,
      });
    } catch (error) {
      console.error('Error sending email:', error);
      throw error;
    }
  }
}

export interface IMailService {
  sendMail<T extends IEmailOptions>(emailOptions: T): Promise<any>;
}
