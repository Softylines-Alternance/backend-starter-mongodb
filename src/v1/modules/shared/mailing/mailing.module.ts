import { Module } from '@nestjs/common';
// import { MailService } from './mail.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { join } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MailerModule } from '@nestjs-modules/mailer';
import { mailServiceProviders } from './providers/mailing.service.providers';
import { MAILING_CONFIG } from 'src/v1/config/mailingConfig';

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async () => ({
        transport: {
          host: MAILING_CONFIG.HOST,
          service: MAILING_CONFIG.SERVICE,
          port: parseInt(MAILING_CONFIG.PORT),
          secure: false,
          auth: {
            user: MAILING_CONFIG.USER,
            pass: MAILING_CONFIG.PASSWORD,
          },
        },
        defaults: {
          from: MAILING_CONFIG.SENDER,
        },
        template: {
          dir: join(__dirname, 'templates'),
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
      }),
      inject: [ConfigService],
    }),
    ConfigModule.forRoot(),
  ],
  providers: [...mailServiceProviders],
  exports: [...mailServiceProviders],
})
export class MailingModule {}
